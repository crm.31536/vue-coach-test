import { createRouter, createWebHistory } from 'vue-router';

import NotFoundVue from '@/pages/NotFound.vue';
import CoachesListVue from '@/pages/coaches/CoachesList.vue';
import CoachDetailVue from '@/pages/coaches/CoachDetail.vue';
import ContactCoachVue from '@/pages/requests/ContactCoach.vue';
// import CoachRegistrationVue from '@/pages/coaches/CoachRegistration.vue';
import RequestsReceivedVue from '@/pages/requests/RequestsReceived.vue';
import UserAuth from '@/pages/auth/UserAuth.vue';
import { store } from '@/store/index';

const CoachRegistrationVue = () => import('@/pages/coaches/CoachRegistration.vue');

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: '/', redirect: '/coaches' },
    { path: '/coaches', component: CoachesListVue },
    {
      path: '/coaches/:id',
      component: CoachDetailVue,
      props: true,
      children: [
        { path: 'contact', component: ContactCoachVue } // /coaches/c1/contact
      ]
    },
    { path: '/register', component: CoachRegistrationVue, meta: { requiresAuth: true } },
    { path: '/requests', component: RequestsReceivedVue, meta: { requiresAuth: true } },
    { path: '/auth', component: UserAuth, meta: { requiresUnAuth: true } },
    { path: '/:notFound(.*)', component: NotFoundVue }
  ]
});

router.beforeEach(function (to, _, next) {
  if (to.meta.requiresAuth && !store.getters.isAuthenticated) {
    next('/auth');
  } else if (to.meta.requiresUnAuth && store.getters.isAuthenticated) {
    next('/coaches');
  } else {
    next();
  }
});

export default router;
