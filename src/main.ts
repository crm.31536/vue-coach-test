import './assets/main.css';

import { createApp } from 'vue';

import App from './App.vue';
import router from './router';
import { store, key } from './store/index';

import components from './components/ui/_index';

const app = createApp(App);

app.use(router);
app.use(store, key);

components.forEach((component) => {
  app.component(component.name, component);
});

app.mount('#app');
