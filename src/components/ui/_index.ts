import BaseBadge from './BaseBadge.vue';
import BaseButton from './BaseButton.vue';
import BaseCard from './BaseCard.vue';
import BaseSpinner from './BaseSpinner.vue';
import BaseDialog from './BaseDialog.vue';

export default [BaseBadge, BaseButton, BaseCard, BaseSpinner, BaseDialog];
