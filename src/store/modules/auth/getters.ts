export default {
  userId(state: { userId: string }) {
    return state.userId;
  },
  token(state: { token: string }) {
    return state.token;
  },
  isAuthenticated(state: { token: string }) {
    return !!state.token;
  },
  didAutoLogout(state: { didAutoLogout: any; }) {
    return state.didAutoLogout;
  }
};
