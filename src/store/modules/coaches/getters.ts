import type { State } from '@/store/index';
import type { Coach } from '@/types/coach';

export default {
  coaches(state: State) {
    return state.coaches;
  },
  hasCoaches(state: any) {
    return state.coaches && state.coaches.length > 0;
  },
  isCoach(_: any, getters: { coaches: any }, _2: any, rootGetters: { userId: any }) {
    const coaches = getters.coaches;
    const userId = rootGetters.userId;
    return coaches.some((coach: Coach) => coach.id === userId);
  }
};
