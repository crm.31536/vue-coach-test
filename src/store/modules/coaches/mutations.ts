import type { State } from '@/store/index';
import type { Coach } from '@/types/coach';

export default {
  registerCoach(state: State, payload: Coach) {
    state.coaches.push(payload);
  },
  setCoaches(state: State, payload: State['coaches']) {
    state.coaches = payload;
  }
};
