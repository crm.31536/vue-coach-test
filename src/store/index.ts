import type { InjectionKey } from 'vue';

import { createStore, Store } from 'vuex';

import coachesModule from './modules/coaches/index';
import requestsModule from './modules/requests/index';
import authModule from './modules/auth/index';

export interface State {
  coaches: {
    id: string;
    firstName: string;
    lastName: string;
    areas: string[];
    description: string;
    hourlyRate: number;
  }[];
}

export const key: InjectionKey<Store<State>> = Symbol();

export const store = createStore({
  modules: {
    coaches: coachesModule,
    requests: requestsModule,
    auth: authModule,
  },
  // state() {
  //   return {
  //     userId: 'c3'
  //   };
  // },
});
